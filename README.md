       _                 _                                               
      | | ___  _   _  __| |_ __   ___  ___ ___  __      ____ ___   _____ 
      | |/ _ \| | | |/ _` | '_ \ / _ \/ __/ __| \ \ /\ / / _` \ \ / / _ \
      | | (_) | |_| | (_| | | | |  __/\__ \__ \  \ V  V / (_| |\ V /  __/
      |_|\___/ \__,_|\__,_|_| |_|\___||___/___/   \_/\_/ \__,_| \_/ \___|
                                                                         
(C) 2021 AGASTYA <me+git@hanabi.in>

Generates a video recording of a .wav file's loudness.  Licensed under GNU AGPLv3 license.  Please see the license file for more information.
![Demo](./demo.gif "Demo")

Usage
=====

1. Use Processing <https://processing.org/download> to run the programme.
1. Update the variables `audioFile` and `videoFile` to refer to the location where the `.wav` (or `.mp3`, `.aiff`) is located and where the videoFile should be saved.
1. Toggle `record` variable to disable recording, when you are just trying out the wave design.  Some audios are not loud and the waves might not look as impressive.  Replace `1` with a smaller fraction on line 41 and 42 for the same, to tweak the size.

Source code details
===================

This repository has a single branch, dev.

+ Submit any patches to the original author under the GNU AGPLv3 license.
+ There is no Contributor License Agreement (CLA) -- the source code will not and cannot be relicensed to any other license other than what is permitted under GNU AGPLv3.
