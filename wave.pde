import processing.sound.*;
import java.util.ArrayList;
import com.hamoid.*;

SoundFile sample;
Amplitude rms;
VideoExport videoExport;

int m = 10;
int dim = 900;
boolean record = true; 
ArrayList<Float> loudness = new ArrayList<Float>();
String audioFile = "/Users/acagastya/documents/repo/wave/sound.wav";
String videoFile = "/Users/acagastya/video.mkv";

public void setup() {
  size(1000, 900);
  sample = new SoundFile(this, audioFile);
  sample.play();
  rms = new Amplitude(this);
  rms.input(sample);
  frameRate(24);
  videoExport = new VideoExport(this, videoFile);
  videoExport.setFrameRate(24);  
  if(record) {
    videoExport.startMovie();
  }
}      

public void draw() {
  textSize(48);
  if(record) {
    videoExport.saveFrame();
  }
  background(0);
  stroke(255);
  strokeWeight(4);
  float vol = rms.analyze();
  loudness.add(vol);
  for(int i = 0; i < loudness.size(); i++) {
    float y1 = map(loudness.get(i), 0, 1, dim/2, dim*0.1);
    float y2 = map(loudness.get(i), 0, 1, dim/2, dim*0.9);
    strokeCap(ROUND);
    line(i*m, dim/2, i*m, y1);
    line(i*m, dim/2, i*m, y2);
  }
  stroke(255, 0, 0);
  strokeWeight(1);
  line(loudness.size() * m, height/2, width, height/2);
  line(loudness.size() * m, 0, loudness.size() * m, height);
  fill(255, 0, 0);
  circle(loudness.size() * m, height/2, 12);
  if (loudness.size() * m > width - 100) {
    loudness.remove(0);
  }
  if(!sample.isPlaying()) {
    noLoop();
    if(record) {
      videoExport.endMovie();
    }
    exit();
  }
}
